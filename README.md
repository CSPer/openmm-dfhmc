# OpenMM DFHMC Python 

## Scope

Provide a DFHMC integrator class for OpenMM. 

The integrator class is created by using the methods of the CustomIntegrator Class. Thus the DFHMCIntegrator inherits from the CUstomIntegrator Class.

## Usage
create an integrator object  as follows
integrator = DFHMCIntegrator(...)

## Features

* HMC Integrator
* applyDigital filter to velocity trajectory
    - Run forward and backwards NVE MD
    - Remove velocity com translation and rotation from each or selected molecule
    - ApplyDigital filter to the internal velocities (com removed velocities)

## Todo



