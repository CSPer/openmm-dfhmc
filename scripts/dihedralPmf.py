import mdtraj as md
import numpy as np
import matplotlib.pyplot as plt
import argparse
import os

def pmf_s(data, nbin, kT):
    density, bins = np.histogram(data, nbin, density=True)
    mids = []
    pmf = []
    for j in range(len(bins)-1):
        if density[j] != 0.0:
            mids.append(bins[j] + 0.5*(bins[j+1] - bins[j]))
            pmf.append(-kT*np.log(density[j]))
    pmf = np.array(pmf)
    return np.array(mids), pmf-min(pmf)

def getDihedral(fpath):
    t = md.load(fpath)
    index_phi = [4,6,8,14]
    index_psi = [6,8,14,16]
    index_dihedral=np.array([index_phi,index_psi])
    dihedral=md.compute_dihedrals(t,index_dihedral, periodic=False)
    return dihedral

def getFes(fpath):
    data = np.loadtxt(fpath)
    data = np.array(zip(*data))
    return data[0], data[1]

def initFigure():
    f = plt.figure(figsize=(10,6), dpi=100)
    ax1=f.add_axes([0.1,0.1,0.35,0.7])
    plt.ylabel('pmf (kJ/mol)')
    plt.xlabel('psi')
    plt.ylim([0,30])
    plt.xlim([-np.pi,np.pi])
    ax2=f.add_axes([0.5,0.1,0.35,0.7])
    plt.ylabel('pmf (kJ/mol)')
    plt.xlabel('phi')
    plt.ylim([0,70])
    plt.xlim([-np.pi,np.pi])
    return ax1,ax2


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Calculate pmf of dihedrals")
    parser.add_argument("-h5", required=True, nargs='*', help="HDF5 trajectory input file")
    parser.add_argument("-Mphi", required=False, help="Metad fes file for phi")
    parser.add_argument("-Mpsi", required=False, help="Metad fes file for psi")
    parser.add_argument("-Uphi", required=False, help="Metad fes file for phi")
    parser.add_argument("-Upsi", required=False, help="Metad fes file for psi")
    args=parser.parse_args()
    h5 = args.h5
    Mphi_path = args.Mphi
    Mpsi_path = args.Mpsi
    Uphi_path = args.Uphi
    Upsi_path = args.Upsi

    kT = 0.008314472471220217*300

    ax1,ax2=initFigure()
    for fpath in h5:
        # get label name from file path
        name = os.path.basename(fpath)

        # Calculate the dihedral angles
        dihedral=getDihedral(fpath)

        # Calculate the pmf
        psi_mids, psi_pmf=pmf_s(dihedral[:,1], 100, kT)
        phi_mids, phi_pmf=pmf_s(dihedral[:,0], 100, kT)

        ax1.plot(psi_mids,psi_pmf ,'.')

        ax2.plot(phi_mids,phi_pmf, '.', label=name)
    
    # Plot results of Metadynamics if present
    if Mpsi_path:
        psi_metad,pmf_psi_metad=getFes(Mpsi_path)
        ax1.plot(psi_metad, pmf_psi_metad, '-')
    if Mphi_path:
        phi_metad,pmf_phi_metad=getFes(Mphi_path)
        ax2.plot(phi_metad, pmf_phi_metad, '-', label='METAD')
    
    # Plot result of Umbrella Sampling if present
    if Upsi_path:
        psi_us,pmf_psi_us=getFes(Upsi_path)
        ax1.plot(psi_us, pmf_psi_us, '-')
    if Uphi_path:
        phi_us,pmf_phi_us=getFes(Uphi_path)
        ax2.plot(phi_us, pmf_phi_us, '-', label='US')

    plt.legend(bbox_to_anchor=(0.1, 0.9),prop={'size':12}, loc=2, borderaxespad=0., frameon=False)
    plt.show()

