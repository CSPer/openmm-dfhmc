import numpy as np
from simtk import unit
from simtk import openmm
from simtk.openmm import app
from openmmtools import testsystems
import matplotlib.pyplot as plt
import os

def get_data_filename(relative_path):
    """Get the full path to one of the reference files in testsystems.
    In the source distribution, these files are in ``openmmtools/data/*/``,
    but on installation, they're moved to somewhere in the user's python
    site-packages directory.
    Parameters
    ----------
    name : str
        Name of the file to load (with respect to the repex folder).
    """

    from pkg_resources import resource_filename
    fn = resource_filename('openmmMC', relative_path)

    if not os.path.exists(fn):
        raise ValueError("Sorry! %s does not exist. If you just added it, you'll have to re-install" % fn)

    return fn


#===============================
# Water molecule 
#===============================

class Water(testsystems.TestSystem):
    def __init__(self, constraints=None, rigid_water=False, **kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        pdb_filename = get_data_filename("systems/water/water.pdb")
        pdbfile = app.PDBFile(pdb_filename)

        forcefields_to_use = ['tip3p.xml']
        forcefield = app.ForceField(*forcefields_to_use)
        system = forcefield.createSystem(pdbfile.topology, constraints=constraints, rigidWater=rigid_water, nonbondedMethod=app.NoCutoff)

        # Get positions.
        positions = pdbfile.getPositions()

        self.system, self.positions, self.topology = system, positions, pdbfile.topology


#===============================
# Abl kinase in Explicit Water
#===============================

class AblExplicit(testsystems.TestSystem):
    """Abl kinase (AMBER 99sb-ildn) in explicit TIP3P solvent using PME electrostatics.
    Parameters
    ----------
    nonbondedMethod : simtk.openmm.app nonbonded method, optional, default=app.PME
       Sets the nonbonded method to use for the water box (CutoffPeriodic, app.Ewald, app.PME).
    Examples
    --------
    >>> abl = AblExplicit()
    >>> system, positions = abl.system, abl.positions
    """

    def __init__(self, constraints=app.HBonds, rigid_water=True, nonbondedMethod=app.PME, nonbondedCutoff=12.0 * unit.angstroms, switch_width=2.0 * unit.angstroms, use_dispersion_correction=True, ewaldErrorTolerance=5E-4, **kwargs):
        testsystems.TestSystem.__init__(self, **kwargs)

        pdb_filename = get_data_filename("data/abl-explicit/2f4j-solvated-minimized-pbccentered.pdb")
        pdbfile = app.PDBFile(pdb_filename)

        # Construct system.
        forcefields_to_use = ['amber99sbildn.xml', 'tip3p.xml']  # list of forcefields to use in parameterization
        forcefield = app.ForceField(*forcefields_to_use)
        system = forcefield.createSystem(pdbfile.topology, constraints=constraints, rigidWater=rigid_water, nonbondedMethod=nonbondedMethod, nonbondedCutoff=nonbondedCutoff)

        # Set dispersion correction use.
        forces = {system.getForce(index).__class__.__name__: system.getForce(index) for index in range(system.getNumForces())}
        forces['NonbondedForce'].setUseDispersionCorrection(use_dispersion_correction)
        forces['NonbondedForce'].setEwaldErrorTolerance(ewaldErrorTolerance)

        if switch_width is not None:
            forces['NonbondedForce'].setUseSwitchingFunction(True)
            forces['NonbondedForce'].setSwitchingDistance(nonbondedCutoff - switch_width)

        # Get positions.
        positions = pdbfile.getPositions()

        self.system, self.positions, self.topology = system, positions, pdbfile.topology

#===============================
# Abl kinase in Implicit Water
#===============================

class AblImplicit(testsystems.TestSystem):

    """Abl kinase in implicit AMBER 99sb-ildn with OBC GBSA solvent.
    Examples
    --------
    >>> abl = AblImplicit()
    >>> system, positions = abl.system, abl.positions
    """

    def __init__(self, **kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        pdb_filename = get_data_filename("data/abl-implicit/2f4j-minimized-boxcentered.pdb")
        pdbfile = app.PDBFile(pdb_filename)

        # Construct system.
        forcefields_to_use = ['amber99sbildn.xml', 'amber99_obc.xml']  # list of forcefields to use in parameterization
        forcefield = app.ForceField(*forcefields_to_use)
        system = forcefield.createSystem(pdbfile.topology, nonbondedMethod=app.NoCutoff, constraints=app.HBonds)

        # Get positions.
        positions = pdbfile.getPositions()

        self.system, self.positions, self.topology = system, positions, pdbfile.topology

#====================================================================
#   Double Well potential along y, Harmonic Oscillator along x and z 
#====================================================================

class DoubleWell(testsystems.TestSystem):
    """Create a 3D harmonic oscillator, with a single particle confined in an isotropic harmonic well.
        Parameters
        ----------
        K_xz : simtk.unit.Quantity, optional, default=1000.0 * unit.kilojoules_per_mole/unit.angstrom**2
            harmonic restraining potential along x and z
        K_y : simtk.unit.Quantity, optional, default=1.0 * unit.kilojoules_per_mole/unit.angstrom**4
            restraining potential along y
        B : simtk.unit.Quantity, optional, default=2.0 * unit.angstrom
            second minima of the double well potential, the first is at 0.0 angstrom
        mass : simtk.unit.Quantity, optional, default=39.948 * unit.amu
            particle mass
        Attributes
        ----------
        system : simtk.openmm.System
            Openmm system with the harmonic oscillator
        positions : list
            positions of harmonic oscillator
        Notes
        -----
        The double well default values is choosen such that the movement along x and z is much faster than along y
        Therefore the integration time step for such a potential needs to be chosen according to the period along x and z
        The movement along x and z is a harmonic oscillator
        The natural period of a harmonic oscillator is T = 2pi*sqrt(m/K), so you will want to use an
        integration timestep smaller than ~ T/10.
        The barrier height along y is Vy = (K_y * B^4) / 16
        For the default values given:
            T = 0.0198 ps
            dt <= 0.00198 ps = 1.98 fs
            Vy = 25.0 kilojoules_per_mole, 10 times higher than 300(K)*kB = 2.5 kilojoules_per_mole
        The standard deviation in position in x and z dimension is sigma = (kT / K)^(1/2)
    """

    def __init__(self, K_xz=40227.6254 * unit.kilojoules_per_mole / unit.angstroms**2, K_y=25 * unit.kilojoules_per_mole / unit.angstroms**4, B=2*unit.angstroms, mass=39.948 * unit.amu, **kwargs):
        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)

        # Set the positions.
        positions = unit.Quantity(np.array([[0.0, 0.0, 0.0]], np.float32), unit.angstroms)

        # Add a restrining potential centered at the origin.
        energy_expression = '(K_xz/2.0) * (x^2 + z^2) + K_y * (y^2) * (y-B)^2;'
        energy_expression += 'K_xz = testsystems_DoubleWell_K_xz;'
        energy_expression += 'K_y = testsystems_DoubleWell_K_y;'
        energy_expression += 'B = testsystems_DoubleWell_B;'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('testsystems_DoubleWell_K_xz', K_xz)
        force.addGlobalParameter('testsystems_DoubleWell_K_y', K_y)
        force.addGlobalParameter('testsystems_DoubleWell_B', B)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.K_xz, self.K_y, self.B, self.mass = K_xz, K_y, B, mass
        self.system, self.positions, self.masses = system, positions, [mass]

        # Number of degrees of freedom.
        self.ndof = 3

    def Potential(self, x, y, z):
        V = (self.K_xz/2.0) * (x**2 + z**2) + self.K_y * (y**2) * (y-self.B)**2
        return V

    def plotPotential(self, x_range=None, y_range=None):
        if y_range is None:
            y_range = [-1, 3, 0.01]
       
        if x_range is None:
            x_range = [-1, 1, 0.01]

        z_range = x_range
        y = np.arange(y_range[0], y_range[2]+y_range[1], y_range[2])*unit.angstroms
        x = np.arange(x_range[0], x_range[2]+x_range[1], x_range[2])*unit.angstroms
        z = np.arange(x_range[0], x_range[2]+x_range[1], x_range[2])*unit.angstroms

        uV=self.Potential(x[0], y[0], z[0]).unit
        ux=x.unit
        Vx =[]
        Vy =[]
        for i in x:
            tmp = 0.0
            for j in z:
                tmp += np.sum(self.Potential(i,y,j))._value
            Vx.append(tmp*z_range[2]*y_range[2])
        
        for i in y:
            tmp=0.0
            for j in z:
                tmp += np.sum(self.Potential(x,i,j))._value
            Vy.append(tmp*z_range[2]*x_range[2])

        normx = np.trapz(Vx, dx = x_range[2])
        normy = np.trapz(Vy, dx = y_range[2])
        Vx = (np.array(Vx)/normx)*uV
        Vy = (np.array(Vy)/normy)*uV
        fig=plt.figure()
        ax1 = fig.add_subplot(2,1,1)
        ax1.plot(y, Vy, 'r-', label='Vy')
        plt.xlabel("y (%s)" % str(ux))
        plt.ylabel("V(y) (%s)" % str(uV))
        ax2 = fig.add_subplot(2,1,2)
        ax2.plot(x, Vx, 'b-', label='Vx')
        plt.xlabel("x (%s)" % str(ux))
        plt.ylabel("V(x) (%s)" % str(uV))
        plt.legend()
        return plt

    @property
    def BarrierHeight(self):
        return (self.K_y * (self.B**4)) / 16.0

    @property
    def Periodxz(self):
        return 2*np.pi*np.sqrt(self.mass/self.K_xz)


#====================================================================
#   Rotated Double Well potential
#====================================================================

class RotatedDoubleWell(testsystems.TestSystem):
    """Create a 3D harmonic oscillator, with a single particle confined in an isotropic harmonic well.
        Parameters
        ----------
        K_xz : simtk.unit.Quantity, optional, default=1000.0 * unit.kilojoules_per_mole/unit.angstrom**2
            harmonic restraining potential along x and z
        K_y : simtk.unit.Quantity, optional, default=1.0 * unit.kilojoules_per_mole/unit.angstrom**4
            restraining potential along y
        B : simtk.unit.Quantity, optional, default=2.0 * unit.angstrom
            second minima of the double well potential, the first is at 0.0 angstrom
        mass : simtk.unit.Quantity, optional, default=39.948 * unit.amu
            particle mass
        Attributes
        ----------
        system : simtk.openmm.System
            Openmm system with the harmonic oscillator
        positions : list
            positions of harmonic oscillator
        Notes
        -----
        The double well default values is choosen such that the movement along x and z is much faster than along y
        Therefore the integration time step for such a potential needs to be chosen according to the period along x and z
        The movement along x and z is a harmonic oscillator
        The natural period of a harmonic oscillator is T = 2pi*sqrt(m/K), so you will want to use an
        integration timestep smaller than ~ T/10.
        The barrier height along y is Vy = (K_y * B^4) / 16
        For the default values given:
            T = 0.0198 ps
            dt <= 0.00198 ps = 1.98 fs
            Vy = 25.0 kilojoules_per_mole, 10 times higher than 300(K)*kB = 2.5 kilojoules_per_mole
        The standard deviation in position in x and z dimension is sigma = (kT / K)^(1/2)
    """

    def __init__(self, degree=45, K_xz=40227.6254 * unit.kilojoules_per_mole / unit.angstroms**2, K_y=25 * unit.kilojoules_per_mole / unit.angstroms**4, B=2*unit.angstroms, mass=39.948 * unit.amu, **kwargs):
        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)

        # Set the positions.
        positions = unit.Quantity(np.array([[0.0, 0.0, 0.0]], np.float32), unit.angstroms)
        theta = degree*np.pi/180
        # Add a restrining potential centered at the origin.
        energy_expression = '(K_xz/2.0) * (xrot^2 + z^2) + K_y * (yrot^2) * (yrot-B)^2;'
        energy_expression += 'xrot = x*cos(theta) + y*sin(theta);'
        energy_expression += 'yrot = -x*sin(theta) + y*cos(theta);'
        energy_expression += 'theta = testsystems_DoubleWell_theta;'
        energy_expression += 'K_xz = testsystems_DoubleWell_K_xz;'
        energy_expression += 'K_y = testsystems_DoubleWell_K_y;'
        energy_expression += 'B = testsystems_DoubleWell_B;'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('testsystems_DoubleWell_theta', theta)
        force.addGlobalParameter('testsystems_DoubleWell_K_xz', K_xz)
        force.addGlobalParameter('testsystems_DoubleWell_K_y', K_y)
        force.addGlobalParameter('testsystems_DoubleWell_B', B)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.K_xz, self.K_y, self.B, self.mass = K_xz, K_y, B, mass
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 3


#====================================================================
#   2D Double Well potential along y, Harmonic Oscillator along x and z 
#====================================================================

class DoubleWell_2D(testsystems.TestSystem):
    """Create a 2D harmonic oscillator, with a single particle confined in an isotropic harmonic well.
        Parameters
        ----------
        K_xz : simtk.unit.Quantity, optional, default=1000.0 * unit.kilojoules_per_mole/unit.angstrom**2
            harmonic restraining potential along x and z
        K_y : simtk.unit.Quantity, optional, default=1.0 * unit.kilojoules_per_mole/unit.angstrom**4
            restraining potential along y
        B : simtk.unit.Quantity, optional, default=2.0 * unit.angstrom
            second minima of the double well potential, the first is at 0.0 angstrom
        mass : simtk.unit.Quantity, optional, default=39.948 * unit.amu
            particle mass
        Attributes
        ----------
        system : simtk.openmm.System
            Openmm system with the harmonic oscillator
        positions : list
            positions of harmonic oscillator
        Notes
        -----
        The double well default values is choosen such that the movement along x and z is much faster than along y
        Therefore the integration time step for such a potential needs to be chosen according to the period along x and z
        The movement along x and z is a harmonic oscillator
        The natural period of a harmonic oscillator is T = 2pi*sqrt(m/K), so you will want to use an
        integration timestep smaller than ~ T/10.
        The barrier height along y is Vy = (K_y * B^4) / 16
        For the default values given:
            T = 0.0198 ps
            dt <= 0.00198 ps = 1.98 fs
            Vy = 25.0 kilojoules_per_mole, 10 times higher than 300(K)*kB = 2.5 kilojoules_per_mole
        The standard deviation in position in x and z dimension is sigma = (kT / K)^(1/2)
    """

    def __init__(self, K_x=2000 * unit.kilojoules_per_mole / unit.angstroms**2, K_y=25 * unit.kilojoules_per_mole / unit.angstroms**4, B=2*unit.angstroms, mass=39.948 * unit.amu, **kwargs):
        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)

        # Set the positions.
        positions = unit.Quantity(np.array([[0.0, 0.0, 0.0]], np.float32), unit.angstroms)

        # Add a restrining potential centered at the origin.
        energy_expression = '(K_x/2.0) * (x^2) + K_y * (y^2) * (y-B)^2;'
        energy_expression += 'K_x = testsystems_DoubleWell_K_x;'
        energy_expression += 'K_y = testsystems_DoubleWell_K_y;'
        energy_expression += 'B = testsystems_DoubleWell_B;'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('testsystems_DoubleWell_K_x', K_x)
        force.addGlobalParameter('testsystems_DoubleWell_K_y', K_y)
        force.addGlobalParameter('testsystems_DoubleWell_B', B)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.K_x, self.K_y, self.B, self.mass = K_x, K_y, B, mass
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 3

    @property
    def BarrierHeight(self):
        return (self.K_y * (self.B**4)) / 16.0

    @property
    def Periodx(self):
        return 2*np.pi*np.sqrt(self.mass/self.K_x)


#====================================================================
#   2D Double Well potential along y, Harmonic Oscillator along x and z 
#====================================================================

class DoubleWell_Curved(testsystems.TestSystem):
    """Create a 2D harmonic oscillator, with a single particle confined in an isotropic harmonic well.
        Parameters
        ----------
        r0 : radius of curvature of the transition path of lowest energy
        a  : distance between the states and the barrier along the transition path
        c  : barrier height
        p  : extend of excursions from the nominal transition path.
        mass : simtk.unit.Quantity, optional, default=39.948 * unit.amu
            particle mass
        Attributes
        ----------
        system : simtk.openmm.System
            Openmm system with the harmonic oscillator
        positions : list
            positions of harmonic oscillator
        Notes
        -----
        
    """

    def __init__(self, 
                 epsilon=0.01,
                 c=25 * unit.kilojoules_per_mole,
                 p=5 * unit.kilojoules_per_mole,
                 r0=1 * unit.angstrom,
                 a = 1 * unit.angstrom*unit.radian,
                 mass=39.948 * unit.amu,**kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)

        # Set the positions.
        positions = unit.Quantity(np.array([[0.1, 0.0, 0.0]], np.float32), unit.angstroms)
       
        # Add a restrining potential centered at the origin.
        energy_expression = 'c * (arc^2-1)^2 + p * B;'
        energy_expression += 'arc = r0*atan(y/x)/a;'
        energy_expression += 'B = (1-epsilon) * R^2 + (3*epsilon - 2) *  R + (1 - 3*epsilon) + epsilon/R;'
        energy_expression += 'R = sqrt(x^2 + y^2)/r0;' 
        energy_expression += 'epsilon = testsystems_DoubleWell_epsilon;'
        energy_expression += 'r0 = testsystems_DoubleWell_r0;'
        energy_expression += 'a = testsystems_DoubleWell_a;'
        energy_expression += 'c = testsystems_DoubleWell_c;'
        energy_expression += 'p = testsystems_DoubleWell_p;'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('testsystems_DoubleWell_epsilon', epsilon)
        force.addGlobalParameter('testsystems_DoubleWell_r0', r0)
        force.addGlobalParameter('testsystems_DoubleWell_a', a)
        force.addGlobalParameter('testsystems_DoubleWell_c', c)
        force.addGlobalParameter('testsystems_DoubleWell_p', p)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.epsilon, self.p, self.c, self.mass = epsilon, p, c, mass
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 3

    @property
    def BarrierHeight(self):
        return self.c

#
# 2D potential w/ curved transition path
#

class Curved_2D(testsystems.TestSystem):
    

    def __init__(self, K=1.0/unit.angstroms**2,
                M=1 * unit.kilojoules_per_mole / unit.angstroms**5,
                N=1 * unit.kilojoules_per_mole,
                mass=39.948 * unit.amu, **kwargs):
        
        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)

        # Set the positions.
        positions = unit.Quantity(np.array([[0.0, -2.0, 0.0]], np.float32), unit.angstroms)

        # Add a restrining potential centered at the origin.
        energy_expression = '-60*M*b*(-x^5- y^5) + 30*N*a -30*N*b + 44;'
        energy_expression += 'a = exp(K*(-(x+1)^2 - (y+1)^2) );'
        energy_expression += 'b = exp(K*(-(x)^2 - (y)^2) );'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('K', K)
        force.addGlobalParameter('M', M)
        force.addGlobalParameter('N', N)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.K, self.M, self.N, self.mass = K, M, N, mass
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 3

class Test_Curved_2D(testsystems.TestSystem):
    

    def __init__(self, K=1.0/unit.angstroms**2,
                M=1 * unit.kilojoules_per_mole,
                mass=39.948 * unit.amu, **kwargs):
        
        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)

        # Set the positions.
        positions = unit.Quantity(np.array([[0.0, 0.0, 0.0]], np.float32), unit.angstroms)

        # Add a restrining potential centered at the origin.
        energy_expression = '-100*M*b + M*100;'
        energy_expression += 'b = exp(K*(-16*(x)^2 - (y)^2) );'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('K', K)
        force.addGlobalParameter('M', M)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.K, self.M, self.mass = K, M, mass
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 3


class Test2_Curved_2D(testsystems.TestSystem):
    

    def __init__(self, K=1.0/unit.angstroms**2,
                M=1 * unit.kilojoules_per_mole,
                l=1.8* unit.angstroms,
                t=0.5*unit.angstroms,
                r=16,
                s=16,
                degree=45,
                mass=39.948 * unit.amu, **kwargs):
        
        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        system.addParticle(mass)
        theta=degree*np.pi/180
        # Set the positions.
        positions = unit.Quantity(np.array([[0.006, -1.855, 0.0]], np.float32), unit.angstroms)

        # Add a restrining potential centered at the origin.
        energy_expression = 'M*(-85*a-100*b-100*c + 99.943);'
        energy_expression += 'a = exp(K*(-r*(xrot+t)^2 - (yrot)^2) );'
        energy_expression += 'b = exp(K*(-s*(x)^2 - (y+l)^2) );'
        energy_expression += 'c = exp(K*(-(x+l)^2 - s*(y)^2) );'
        energy_expression += 'xrot = x*cos(theta) + y*sin(theta);'
        energy_expression += 'yrot = -x*sin(theta) + y*cos(theta);'
        force = openmm.CustomExternalForce(energy_expression)
        force.addGlobalParameter('K', K)
        force.addGlobalParameter('M', M)
        force.addGlobalParameter('l', l)
        force.addGlobalParameter('s', s)
        force.addGlobalParameter('t', t)
        force.addGlobalParameter('r', r)
        force.addGlobalParameter('theta', theta)
        force.addParticle(0, [])
        system.addForce(force)

        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('Ar')
        chain = topology.addChain()
        residue = topology.addResidue('DW', chain)
        topology.addAtom('Ar', element, residue)
        self.topology = topology

        self.K, self.M, self.mass = K, M, mass
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 3


#
# test water
#

class testWater(testsystems.TestSystem):
    """
    """

    def __init__(self,
                 theta0=1.82421813418*unit.radian, k_angle=460.24*unit.kilojoules_per_mole/unit.radian**2,
                 r0=0.09572*unit.nanometer, k_bond=376560*unit.kilojoules_per_mole/unit.nanometer**2,
                 mass_H=1.007947*unit.amu,
                 mass_O=15.99943*unit.amu,
                 **kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        atom_O = system.addParticle(mass_O)
        atom_H1 = system.addParticle(mass_H)
        atom_H2 = system.addParticle(mass_H)

        # Initial Configuration.
        positions = unit.Quantity(np.array([[-0.3730, -0.9368, 0.2927],
                                            [-0.4276, -0.8584, 0.2877],
                                            [-0.3991, -0.9791, 0.3745]], np.float32), unit.nanometer)
        
        # Build the Force Field
        
        # Bonds
        bondForce = openmm.HarmonicBondForce()
        bondForce.addBond(atom_O, atom_H1, r0, k_bond)
        bondForce.addBond(atom_O, atom_H2, r0, k_bond)

        # Angles
        theta0 = theta0.in_units_of(unit.radian)
        
        angleForce = openmm.HarmonicAngleForce()
        angleForce.addAngle(atom_H1, atom_O, atom_H2, theta0, k_angle)
    
        # Remove Com Translation
        #CMMotion = openmm.CMMotionRemover(1)

        system.addForce(bondForce)
        system.addForce(angleForce)
        #system.addForce(CMMotion)
        
        # Create topology.
        topology = app.Topology()
        element_O = app.Element.getBySymbol('O')
        element_H = app.Element.getBySymbol('H')
        chain = topology.addChain()
        residue = topology.addResidue('WAT', chain)
        atom_O = topology.addAtom('O', element_O, residue)
        atom_H1 = topology.addAtom('H1', element_H, residue)
        atom_H2 = topology.addAtom('H2', element_H, residue)

        topology.addBond(atom_O, atom_H1)
        topology.addBond(atom_O, atom_H2)
        
        self.topology = topology
        self.system, self.positions = system, positions
        self.masses = np.array([mass_O, mass_H, mass_H])

        # Number of degrees of freedom.
        self.ndof = 9



#====================================================================
#   Dihedral Potential
#====================================================================

class Dihedral(testsystems.TestSystem):
    """
    """

    def __init__(self,
                 periodicity=2, phase=0*unit.degree ,k_torsion=12.5*unit.kilojoules_per_mole,
                 theta0=95*unit.degree, k_angle=10000*unit.kilojoules_per_mole/unit.radian**2,
                 r0=0.153*unit.nanometer, k_bond=2200000*unit.kilojoules_per_mole/unit.nanometer**2,
                 mass1=15.0035*unit.amu,
                 mass2=14.027*unit.amu,
                 **kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        atom1 = system.addParticle(mass1)
        atom2 = system.addParticle(mass2)
        atom3 = system.addParticle(mass2)
        atom4 = system.addParticle(mass1)

        # Initial Configuration.
        r0_value = r0.value_in_unit(unit.nanometer)
        positions = unit.Quantity(np.array([[0.0, 0.0, 0.0],
                                            [0.0, r0_value, 0.0],
                                            [r0_value, r0_value, 0.0],
                                            [r0_value, r0_value, r0_value]], np.float32), unit.nanometer)
        
        # Build the Force Field
        
        # Torsion
        phase = phase.in_units_of(unit.radian)
        
        torsionForce = openmm.PeriodicTorsionForce()
        torsionForce.addTorsion(atom1, atom2, atom3, atom4, periodicity, phase, k_torsion)
        
        # Bonds
        bondForce = openmm.HarmonicBondForce()
        bondForce.addBond(atom1, atom2, r0, k_bond)
        bondForce.addBond(atom2, atom3, r0, k_bond)
        bondForce.addBond(atom3, atom4, r0, k_bond)

        # Angles
        theta0 = theta0.in_units_of(unit.radian)
        
        angleForce = openmm.HarmonicAngleForce()
        angleForce.addAngle(atom1, atom2, atom3, theta0, k_angle)
        angleForce.addAngle(atom2, atom3, atom4, theta0, k_angle)
    
        # Remove Com Translation
        CMMotion = openmm.CMMotionRemover(1)

        system.addForce(torsionForce)
        system.addForce(bondForce)
        system.addForce(angleForce)
        system.addForce(CMMotion)

        # Add Constraints
        # system.addConstraint(atom1, atom2, r0)
        # system.addConstraint(atom2, atom3, r0)
        # system.addConstraint(atom3, atom4, r0)
        
        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('C')
        chain = topology.addChain()
        residue = topology.addResidue('BUT', chain)
        atom1 = topology.addAtom('CH3', element, residue)
        atom2 = topology.addAtom('CH2', element, residue)
        atom3 = topology.addAtom('CH2', element, residue)
        atom4 = topology.addAtom('CH3', element, residue)

        topology.addBond(atom1, atom2)
        topology.addBond(atom2, atom3)
        topology.addBond(atom3, atom4)
        
        self.topology = topology

        
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 9


class Dihedral2(testsystems.TestSystem):
    """
    """

    def __init__(self,
                 V0=0.0*unit.kilojoules_per_mole, V1=6.362*unit.kilojoules_per_mole,
                 V2=-1.317*unit.kilojoules_per_mole, V3=13.405*unit.kilojoules_per_mole,
                 periodicity=2, phase=0*unit.degree ,k_torsion=12.5*unit.kilojoules_per_mole,
                 theta0=95*unit.degree, k_angle=10000*unit.kilojoules_per_mole/unit.radian**2,
                 r0=0.153*unit.nanometer, k_bond=2200000*unit.kilojoules_per_mole/unit.nanometer**2,
                 mass1=15.0035*unit.amu,
                 mass2=14.027*unit.amu,
                 **kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        atom1 = system.addParticle(mass1)
        atom2 = system.addParticle(mass2)
        atom3 = system.addParticle(mass2)
        atom4 = system.addParticle(mass1)

        # Initial Configuration.
        r0_value = r0.value_in_unit(unit.nanometer)
        positions = unit.Quantity(np.array([[0.0, 0.0, 0.0],
                                            [0.0, r0_value, 0.0],
                                            [r0_value, r0_value, 0.0],
                                            [r0_value, r0_value, r0_value]], np.float32), unit.nanometer)
        
        # Build the Force Field
     
        energy_expression = 'V0 + 0.5*V1*(1+cos(theta)) + 0.5*V2*(1-cos(2*theta)) + 0.5*V3*(1+cos(3*theta));'
        force = openmm.CustomTorsionForce(energy_expression)
        force.addGlobalParameter('V0',V0)
        force.addGlobalParameter('V1',V1)
        force.addGlobalParameter('V2',V2)
        force.addGlobalParameter('V3',V3)
        force.addTorsion(atom1, atom2, atom3, atom4, [])
        # Torsion
        phase = phase.in_units_of(unit.radian)
        
        torsionForce = openmm.PeriodicTorsionForce()
        torsionForce.addTorsion(atom1, atom2, atom3, atom4, periodicity, phase, k_torsion)
        
        # Bonds
        bondForce = openmm.HarmonicBondForce()
        bondForce.addBond(atom1, atom2, r0, k_bond)
        bondForce.addBond(atom2, atom3, r0, k_bond)
        bondForce.addBond(atom3, atom4, r0, k_bond)

        # Angles
        theta0 = theta0.in_units_of(unit.radian)
        
        angleForce = openmm.HarmonicAngleForce()
        angleForce.addAngle(atom1, atom2, atom3, theta0, k_angle)
        angleForce.addAngle(atom2, atom3, atom4, theta0, k_angle)
    
        # Remove Com Translation
        CMMotion = openmm.CMMotionRemover(1)

        system.addForce(force)
        system.addForce(bondForce)
        system.addForce(angleForce)
        #system.addForce(CMMotion)

        # Add Constraints
        # system.addConstraint(atom1, atom2, r0)
        # system.addConstraint(atom2, atom3, r0)
        # system.addConstraint(atom3, atom4, r0)
        
        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('C')
        chain = topology.addChain()
        residue = topology.addResidue('BUT', chain)
        atom1 = topology.addAtom('CH3', element, residue)
        atom2 = topology.addAtom('CH2', element, residue)
        atom3 = topology.addAtom('CH2', element, residue)
        atom4 = topology.addAtom('CH3', element, residue)

        topology.addBond(atom1, atom2)
        topology.addBond(atom2, atom3)
        topology.addBond(atom3, atom4)
        
        self.topology = topology

        
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 9


#====================================================================
#   Harmonic Bond Potential
#====================================================================

class HarmonicOscillator(testsystems.TestSystem):
    """
    """

    def __init__(self,
                 r0=0.153*unit.nanometer, k=5000*unit.kilojoules_per_mole/unit.nanometer**2,
                 mass1=15.0035*unit.amu,
                 mass2=14.027*unit.amu,
                 **kwargs):

        testsystems.TestSystem.__init__(self, **kwargs)

        # Create an empty system object.
        system = openmm.System()

        # Add the particle to the system.
        atom1 = system.addParticle(mass1)
        atom2 = system.addParticle(mass2)

        # Initial Configuration.
        r0_value = r0.value_in_unit(unit.nanometer)
        positions = unit.Quantity(np.array([[0.0, -0.5*r0_value, 0.0],
                                            [0.0, 0.5*r0_value, 0.0]], np.float32), unit.nanometer)
        
        # Build the Force Field
        
        # Bonds
        bondForce = openmm.HarmonicBondForce()
        bondForce.addBond(atom1, atom2, r0, k)

        # Remove Com Translation
        CMMotion = openmm.CMMotionRemover(1)

        system.addForce(bondForce)
        #system.addForce(CMMotion)

        # Add Constraints
        # system.addConstraint(atom1, atom2, r0)
        # system.addConstraint(atom2, atom3, r0)
        # system.addConstraint(atom3, atom4, r0)
        
        # Create topology.
        topology = app.Topology()
        element = app.Element.getBySymbol('C')
        chain = topology.addChain()
        residue = topology.addResidue('OSC', chain)
        atom1 = topology.addAtom('CH3', element, residue)
        atom2 = topology.addAtom('CH2', element, residue)

        topology.addBond(atom1, atom2)
        
        self.topology = topology

        
        self.system, self.positions = system, positions

        # Number of degrees of freedom.
        self.ndof = 6
