import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import simtk.openmm as openmm
import numpy as np
import mdtraj

def psd(signal, dt, padding=50):
    """
        Given a signal and timestep
        The function calculates the Power Spectrum
        of the signal and returns the frequencies and 
        the signal in freq space
    """
    N = len(signal)
    P, freq = mlab.psd(signal, N, 1.0/dt)
    P = P/len(P)
    return freq, P

def deg2rad(degree):
    """
        Converts degree to radians
    """
    radian = degree*(np.pi/180.0)
    return radian

def rad2deg(radian):
    """
        Converts radian to degree
    """
    degree = radian*(180.0/np.pi)
    return degree

def sin(N, dt, frequency, phase):
    """
        frequency: the frequency of the sinvawe
        dt       : sampling timestep
        N        : total sampling points
        phase    : phase in degrees
        Returns a signwave (time, amplitude)
    """
    phi = deg2rad(phase)
    
    t = np.arange(0,N*dt,dt)

    w = 2*np.pi*frequency

    return t, np.sin(w*t + phi)

def FIR(signal, coeff):
    """
        Freq response for FIR filter
        returns:
        the ratio of the filtered amplitute w.r.t to the unfiltered
    """

    np=len(coeff)

    assert len(signal) >= np

    y = sum(signal[0:np]*coeff)
    amp = abs(y / signal[(np-1)/2])
    return amp

def FIRavg(signal, np):
    """
        Frequency response for moving average
        returns: 
        the ratio of the filtered amplitute w.r.t to the unfiltered
    """

    assert len(signal) >= np

    y = sum(signal[0:np])/float(np)
    amp = abs(y / signal[(np)/2])
    return amp

def FResponse(coeff, L=14):
    """Calculate and plot filter response
       for moving average and FIR filter

    """

    # Set Signal variables
    nSample = len(coeff) # number of points in a signal
    dt = 0.001  # timestep of signal
    f = 0.01    # starting frequency
    
    df = 0.1    # frequency step
    
    f_nyg = 1.0/(2*dt) # Nyquist frequency (Max frequency that can be observed in FT)
   
    NSignals = 2000     # Number of signals to which DF will be applied
    
    amp = []
    ampAvg = []
    freq = []
    
    for i in xrange(NSignals):
        # generate signals
        t, y = sin(nSample, dt, f, 0)

        ampAvg.append(FIRavg(y,L))
        amp.append(FIR(y,coeff))
        
        freq.append(f/f_nyg)    # stores the relative frequencies range [0,1]
        f = f + df

    # Plot the freq responses
    f =  plt.figure(figsize=(10,6), dpi=100)
    plt.tick_params(axis='y', labelsize=15)
    plt.tick_params(axis='x', labelsize=15)
    plt.xlim([0,0.5])
    plt.xlabel('$f_{normalised} (f/f_{Nyquist})$', size=20)
    plt.ylabel('$A_{filtered}/A_{signal}$', size=20)
    plt.plot(freq, amp, 'r', label='FIR')
    plt.plot(freq, ampAvg, 'b', label='moving avg')
    plt.legend()
    plt.suptitle('$Frequency \quad Response$', size=20)
    return plt

def pmf_s(data, nbin, kT, binRange=None):
    """
        data : 1d numpy array or list that holds trajectory info
            example data could be the Phi dihedral angle
        nbin : The number of bins to be used when calcuating the histogram

        ToDo: specifiy the bins exactly by given a range
            This will enable to calculate error over multiple runs.
    """

    density, bins = np.histogram(data, nbin, binRange, density=True)
    mids = []
    pmf = []
    for j in range(len(bins)-1):
        # if density is zero (no data at that bin) omit, otherwise the log will be infinite
        if density[j] != 0.0:
            mids.append(bins[j] + 0.5*(bins[j+1] - bins[j]))
            pmf.append(-kT*np.log(density[j]))
    pmf = np.array(pmf)
    return np.array(mids), pmf-min(pmf)

def getMETAD(fout):
    """
        Read metdaynamics output file
        two columns
            first one is the respective Collective variable
            the second is the pmf of that variable
    """
    data = np.loadtxt(fout)
    data = np.array(zip(*data))
    pmf = data[1]
    return data[0], pmf

def readTrj(fout, collection, label, index_phi=None, index_psi=None):
    """
        Read h5 file and collect several info into 
        the dictionary collection
    """

    f=mdtraj.formats.HDF5TrajectoryFile(fout, mode='r')
    t=f.read()
    f.close()
    
    traj = mdtraj.load(fout)
    
    # default dihedral angles are the phi and psi angles of alanineDipeptide
    if index_phi is None:
        index_phi = [4,6,8,14]
    if index_psi is None:
        index_psi = [6,8,14,16]

    index_dihedral = np.array([index_phi, index_psi])
    dihedral = mdtraj.compute_dihedrals(traj, index_dihedral, periodic=True, opt=True)
    time = t.time

    #check if velocity info is present
    try:
        t.velocities[:,0,0]
        vel = True
    except:
        vel = False
    
    collection[label] = {'time' : time, 'positions' : t.coordinates, 'phi' : dihedral[:,0], 'psi': dihedral[:,1]}
    if vel:
        collection['velocities'] = t.velocities
    return collection

def selectAtom(natoms, index=None):
    """
        atoms: vector<Vec3> len(atoms) = natoms
        index: list<int> len(index) <= natoms
        returns a vector<Vec3> with length natoms, 
        the values are all zero except the ones given by the index
    """
    if index is None:
        index = []
    
    assert len(index) <= natoms
    
    atoms = []
    # set all atoms to 0
    for i in range(natoms):
        atoms.append(openmm.vec3.Vec3(0.0,0.0,0.0))
    
    # set atoms with relevant index to 1
    for j in index:
        atoms[j] = openmm.vec3.Vec3(1.0,1.0,1.0)
    return atoms
