import simtk.openmm as mm
import simtk.unit as units
import numpy as np
import time

kB = units.BOLTZMANN_CONSTANT_kB * units.AVOGADRO_CONSTANT_NA

class Output():
    def __init__(self):
        self.positions = []
        self.velocities = []
        self.time = []
        self.temperature = None
        self.Ekin = None
        self.Epot = None

    def add(self, out):
        self.positions += out.positions
        self.velocities += out.velocities

class test(mm.CustomIntegrator):
    def __init__(self, coeff, temperature, timestep):
        super(test, self).__init__(timestep)

        kT = kB * temperature
        self.addPerDofVariable("testdof1", 0)
        self.addPerDofVariable("testdof2",0)
        self.addPerDofVariable("x1", 0) # used for contraint

        # Filter related variables
        self.addPerDofVariable("vfilt", 0)
        self.addPerDofVariable("vint", 0)
        self.addPerDofVariable("vmid", 0)
        self.addGlobalVariable("c", 0)

        self.addPerDofVariable("x0", 0)
        self.addPerDofVariable("v0", 0)
        self.addPerDofVariable("sigma", 0)
        self.addGlobalVariable("kT", kT)
	self.addComputePerDof("sigma", "sqrt(kT/m)")

        # Draw velocities from Normal distro with mean 0 and std=sigma
        self.addComputePerDof("v", "sigma*gaussian")
        self.addConstrainVelocities()

        self.applyDF(coeff)
        self.addComputePerDof("testdof1","x")
        self.MD(10)
        self.addComputePerDof("testdof2","x")


    def MD(self, nsteps):
        """
            Velocity Verlet integrator
        """
        for step in range(nsteps):
            self.addComputePerDof("v", "v+0.5*dt*f/m")
            self.addComputePerDof("x", "x+dt*v")
            self.addComputePerDof("x1", "x")
            self.addConstrainPositions()
            self.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
            self.addConstrainVelocities()

    def applyDF(self, coeff):
        """
            Calculate vfilt
            by taking the linear combination
            of the each velocities with the given coefficients
        """

        # TODO: Remove com motion from velocities and position
        # before filter aplication
        # Something is going on here investigate. the forces at the end of the Df changes 
        # when run one after other
        # also the forces before the application fdoes not match after the application

        self.addComputePerDof("x0", "x")
        self.addComputePerDof("v0", "v")
        Nbuffer= len(coeff)
        self.addComputePerDof("vfilt", "0")

        for i in range(Nbuffer):
            self.MD(1)
            self.addRemoveCom("vint")
            # Remove translation and rotation from velocities
            self.addComputeGlobal("c", str(coeff[i]))
            self.addComputePerDof("vfilt", "vfilt + vint*c")
            if i == (Nbuffer-1)/2:
                self.addComputePerDof("vmid","v")

        self.addComputePerDof("x", "x0")
        self.addComputePerDof("v", "v0")

    @property
    def vmid(self):
        return self.getPerDofVariableByName("vmid")

    @property
    def vfilt(self):
        return self.getPerDofVariableByName("vfilt")

    @property
    def testdof1(self):
        return self.getPerDofVariableByName("testdof1")
 
    @property
    def testdof2(self):
        return self.getPerDofVariableByName("testdof2")


class DFIntegrator(mm.CustomIntegrator):
    # Too Slow!!
    def __init__(self, coeff, timestep=1*units.femtoseconds):
        super(DFIntegrator, self).__init__(timestep)
        Nbuffer = len(coeff)
        DFpart1 = (Nbuffer+1)/2
        self.DFpart1 = DFpart1
        self.addPerDofVariable("x1", 0)  # for constraints
        self.addPerDofVariable("vfilt", 0)
        self.addGlobalVariable("c", 0)
        self.addGlobalVariable("nstep", 0)
        
        # Create variables to store the first part of DF
        for i in range(DFpart1):
            self.addPerDofVariable("x_"+str(i), 0)
            self.addPerDofVariable("v_"+str(i), 0)
            self.addGlobalVariable("Ekin_"+str(i), 0)
            self.addGlobalVariable("Epot_"+str(i), 0)
            self.addGlobalVariable("Etot_"+str(i), 0)
        
        for i in range(Nbuffer):
            self.MD()
            self.addComputeGlobal("c", str(coeff[i]))
            self.addComputePerDof("vfilt", "vfilt + v*c")
            if i < DFpart1:
                self.addComputePerDof("x_"+str(i), "x")
                self.addComputePerDof("v_"+str(i), "v")
                self.addComputeSum("Ekin_"+str(i), "0.5*m*v*v")
                self.addComputeGlobal("Epot_"+str(i), "energy")
                self.addComputeGlobal("Etot_"+str(i), "Ekin_"+str(i)+" + energy")

        # Restore state to the middle of buffer
        self.addComputePerDof("x","x_"+str((Nbuffer-1)/2))
        self.addComputePerDof("v","vfilt")
        self.addComputePerDof("v_"+str((Nbuffer-1)/2),"vfilt")
        self.addUpdateContextState()

        # keep track of trj and energies
        self.addComputeSum("Ekin_"+str((Nbuffer-1)/2), "0.5*m*v*v")
        self.addComputeGlobal("Etot_"+str((Nbuffer-1)/2), "Ekin_"+str((Nbuffer-1)/2)+" + "+"Ekin_"+str((Nbuffer-1)/2))

    def MD(self):
        self.addComputePerDof("v", "v+0.5*dt*f/m")
        self.addComputePerDof("x", "x+dt*v")
        self.addComputePerDof("x1", "x")
        self.addConstrainPositions()
        self.addComputePerDof("v", "v+0.5*dt*f/m+(x-x1)/dt")
        self.addConstrainVelocities()

    @property
    def getx(self):
        """Get the value of alpha for the integrator."""
        x = []
        for i in range(self.DFpart1):
            x.append(self.getPerDofVariableByName("x_"+str(i)))
        return x

    @property
    def getv(self):
        """Get the value of alpha for the integrator."""
        v = []
        for i in range(self.DFpart1):
            v.append(self.getPerDofVariableByName("v_"+str(i)))
        return v

    @property
    def getEpot(self):
        """Get the value of alpha for the integrator."""
        Epot = []
        for i in range(self.DFpart1):
            Epot.append(self.getGlobalVariableByName("Epot_"+str(i)))
        return Epot

    @property
    def getEkin(self):
        """Get the value of alpha for the integrator."""
        Ekin = []
        for i in range(self.DFpart1):
            Ekin.append(self.getGlobalVariableByName("Ekin_"+str(i)))
        return Ekin

    @property
    def getEtot(self):
        """Get the value of alpha for the integrator."""
        Etot = []
        for i in range(self.DFpart1):
            Etot.append(self.getGlobalVariableByName("Etot_"+str(i)))
        return Etot

    @property
    def getc(self):
        c = []
        for i in range(self.DFpart1):
            c.append(self.getPerDofVariableByName("c_"+str(i)))
        return c

    @property
    def getvfilt(self):
        return self.getPerDofVariableByName("vfilt")
