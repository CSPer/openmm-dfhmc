import mdtraj

def HDF5Reporter(fname, coordinates, velocities=None, time=None):
    f = mdtraj.formats.HDF5TrajectoryFile(fname, mode='w')
    f.write(coordinates, velocities=velocities, time=time)
    f.close()