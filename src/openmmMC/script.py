import numpy as np
from simtk import unit
from simtk import openmm
from simtk.openmm import app

import openmmMC.integrators
import openmmMC.reporters
import openmmMC.testsystems

import openmmtools.integrators
import openmmtools.testsystems

import mdtraj

import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

# Global Variables
kB = unit.BOLTZMANN_CONSTANT_kB * unit.AVOGADRO_CONSTANT_NA

def getDFCoeff(fpath):
    """
        Given the fpath, it reads each line and 
        returns it as a list
    """
    coeff = []
    with open(fpath, 'r') as f:
        lines=f.readlines()
    for line in lines:
        coeff.append(np.float32(line.strip()))
    return coeff

def fft(signal, dt):
    """
        Given a signal and timestep
        The function calculates the Fourier transform
        of the signal and returns the frequencies and 
        the signal in freq space
    """

    w = np.fft.rfft(signal)
    freq = np.fft.rfftfreq(len(signal), d=dt)
    return freq, abs(w)

def psd(signal, dt, padding=50):
    """
        Given a signal and timestep
        The function calculates the Power Spectrum
        of the signal and returns the frequencies and 
        the signal in freq space
    """
    N = len(signal)
    P, freq = mlab.psd(signal, N, 1.0/dt)
    return freq, P

def magnitude_spectrum(signal, dt):
    """
        Given a signal and timestep
        The function calculates the Magnitude Spectrum
        of the signal and returns the frequencies and 
        the signal in freq space
    """
    N = len(signal)
    A, freq = mlab.magnitude_spectrum(signal, Fs=1.0/dt)
    return freq, A

def phase_spectrum(signal, dt):
    """
        Given a signal and timestep
        The function calculates the Phase Spectrum
        of the signal and returns the frequencies and 
        the Phase in Radians
    """
    N = len(signal)
    phase, freq = mlab.phase_spectrum(signal, Fs=1.0/dt)
    return freq, phase

def MDFreq(fname, cut=None, padding=0):

    """
        Given a h5 trajectory file, it plot the 
        positions and velocitiy time signals.
        It also calculates the Power spectrum of the 
        total velocity signal and plots it.

        cut: can be used to cut the signal to be anlysed
        by power spectrum in twice.
        padding: specify the number of steps to be skipped after the first cut.
    """

    f=mdtraj.formats.HDF5TrajectoryFile(fname, mode='r')
    t=f.read()
    f.close()
    # all frames 0th atom ith coordinate
    time = t.time
    timestep = t.time[1]-t.time[0] # dt in picoseconds
    signal_x = []
    signal_v = []
    for i in range(3):
        signal_v.append(t.velocities[:,0,i])
        signal_x.append(t.coordinates[:,0,i])

    # Prepare the signal
    # have two signal if a cut point is defined
    # This used to analyze the signal before and after filter application
    signal = []
    if cut is not None:
        signal.append(signal_v[1][:cut]+signal_v[0][:cut]+signal_v[2][:cut])
        signal.append(signal_v[1][cut+padding:]+signal_v[0][cut+padding:]+signal_v[2][cut+padding:])
    else:
        signal.append(signal_v[1]+signal_v[0]+signal_v[2])

    w = []
    wavenumber = []
    freq = []
    for i in range(len(signal)):
        # for each signal calculate the power spectrum
        freq0, w0 =psd(signal[i], timestep)
        w.append(w0/len(w0)) # scale the amplitute so that maximum amp is 1
        wavenumber.append(freq0*100/3.0) # scale the freq to be 1/cm
        freq.append(freq0/1000.0) # scale the freq to be 1/fs

    f = plt.figure(figsize=(10,6), dpi=100)
    # Spectral Density - 1/cm Wavenumber
    ax1=f.add_axes([0.1,0.1,0.35,0.35])
    for i in range(len(w)):
        plt.plot(wavenumber[i], w[i], label='w_' + str(i))
    #plt.ylim([0,2])
    plt.xlim([0,6000])
    plt.xlabel('Frequency (1/cm)')
    plt.ylabel('Spectral density')
    plt.legend(bbox_to_anchor=(0.1, 0.9),prop={'size':12}, loc=2, borderaxespad=0., frameon=False)

    # Spectral Density - Frequecny 1/fs
    ax2=f.add_axes([0.1,0.55,0.35,0.35])
    for i in range(len(w)):
        plt.plot(freq[i], w[i], label='w_' + str(i))
    #plt.ylim([0,2])
    plt.xlim([0,0.2])
    plt.xlabel('Frequency (1/fs)')
    plt.ylabel('Spectral density')
    plt.legend(bbox_to_anchor=(0.1, 0.9),prop={'size':12}, loc=2, borderaxespad=0., frameon=False)

    # Spectral Density - normalised Frequency
    ax3=f.add_axes([0.55,0.55,0.35,0.35])
    for i in range(len(w)):
        plt.plot(freq[i]/max(freq[i]), w[i], label='w_' + str(i))
    #plt.ylim([0,2])
    plt.xlim([0,0.05])
    plt.xlabel('Frequency/F_{nyquist}')
    plt.ylabel('Spectral density')
    plt.legend(bbox_to_anchor=(0.1, 0.9),prop={'size':12}, loc=2, borderaxespad=0., frameon=False)

    # Time series plot of the signal components
    ax3=f.add_axes([0.55,0.1,0.35,0.35])
    plt.plot(time, signal_v[0], label='vx')
    plt.plot(time, signal_v[1], label='vy')
    plt.plot(time, signal_v[2], label='vz')
    plt.xlabel('Time (ps)')
    plt.ylabel('nm/ps')
    plt.legend(bbox_to_anchor=(0.1, 0.9),prop={'size':12}, loc=2, borderaxespad=0., frameon=False)

    f2 = plt.figure(figsize=(10,6), dpi=100)
    plt.plot(time, signal_x[0], label='x')
    plt.plot(time, signal_x[1], label='y')
    plt.plot(time, signal_x[2], label='z')
    plt.xlabel('Time (ps)')
    plt.ylabel('nm')
    plt.legend(bbox_to_anchor=(0.1, 0.9),prop={'size':12}, loc=2, borderaxespad=0., frameon=False)
    plt.show()


def NVE(MDSteps, dt=1*unit.femtoseconds, testsystem=None, fout='NVEtest.h5'):
    if testsystem is None:
        testsystem = openmmMC.testsystems.DoubleWell()
        #testsystem = openmmtools.testsystems.HarmonicOscillator(K=50000.0*unit.kilojoules_per_mole / unit.angstroms**2)

    system, topology = testsystem.system, testsystem.topology
    temperature = 300.0 * unit.kelvin

    integrator = openmmMC.integrators.VelocityVerletIntegrator(timestep=dt)
    integrator.setRandomNumberSeed(12343245)
    simulation = app.Simulation(topology, system, integrator)
    simulation.context.setPositions(testsystem.positions)
    simulation.context.setVelocitiesToTemperature(temperature, 23452367)
    
    simulation.reporters.append(mdtraj.reporters.HDF5Reporter(fout, 1, velocities=True))

    simulation.step(MDSteps)

def DFMD(fname, delay, testsystem=None, scl=1.0, fout='testLowEnhanceDFMDout.h5'):
    MDStep=2000
    if testsystem is None:
        testsystem = openmmMC.testsystems.DoubleWell()
        #testsystem = openmmtools.testsystems.HarmonicOscillator(K=50000.0*unit.kilojoules_per_mole / unit.angstroms**2)
    system, topology = testsystem.system, testsystem.topology
    temperature = 300.0 * unit.kelvin

    coeff = getDFCoeff(fname)
    
    #coeff = [1,2,3,4,5,6,7,8,9,10,11]
    integrator = openmmMC.integrators.VelocityVerletIntegrator(timestep=0.1*unit.femtoseconds)
    integrator.setRandomNumberSeed(12343245)
    simulation = app.Simulation(topology, system, integrator)
    simulation.context.setPositions(testsystem.positions)
    #simulation.context.setVelocities(unit.Quantity(np.array([[0.0, 0.1, 0.0]], np.float32), unit.angstroms/unit.picoseconds))
    simulation.context.setVelocitiesToTemperature(temperature, 23452367)
    
    # simulation.reporters.append(app.StateDataReporter(stdout, 1, step=True))
    # 25 delay for target ~0.1 1/fs at 0.1 fs dt
    # 625 delay for target ~0.004 1/fs at 0.1 dt
    output=openmmMC.integrators.applyDF(coeff, delay,simulation, integrator, scl=1.0)
    #output.add(openmmMC.integrators.applyDF(coeff, delay,simulation, integrator, scl=1.0))
    
    for i in range(MDStep):
        simulation.step(1)
        output.positions.append(integrator.getx)
        output.velocities.append(integrator.getv)

    dt = 0.1*(10**-3)
    time_len = MDStep + ((len(coeff)+1)/2 + sum(np.array(delay)))
    end = time_len*dt
    output.time = np.linspace(dt, end, num=time_len, endpoint=True)
    openmmMC.reporters.HDF5Reporter(fout, output.positions, velocities=output.velocities, time=output.time)

def RDFMD(fname):
    MDStep=1000
    testsystem = openmmMC.testsystems.DoubleWell()
    #testsystem = testsystems.HarmonicOscillator(K=50000.0*unit.kilojoules_per_mole / unit.angstroms**2)
    system, topology = testsystem.system, testsystem.topology
    temperature = 300.0 * unit.kelvin

    coeff = getDFCoeff(fname)
    
    #coeff = [1,2,3,4,5,6,7,8,9,10,11]
    integrator = openmmMC.integrators.VelocityVerletIntegrator(timestep=1*unit.femtoseconds)
    integrator.setRandomNumberSeed(12343245)
    simulation = app.Simulation(topology, system, integrator)
    simulation.context.setPositions(testsystem.positions)
    simulation.context.setVelocities(unit.Quantity(np.array([[0.0, 0.1, 0.0]], np.float32), unit.angstroms/unit.picoseconds))
    #simulation.context.setVelocitiesToTemperature(temperature, 23452367)
    
    # simulation.reporters.append(app.StateDataReporter(stdout, 1, step=True))
    trjx =[]
    for i in range(MDStep):
        simulation.step(1)
        trjx.append(integrator.getx)

    delay = 0
    tmp=openmmMC.integrators.applyRDF(coeff,delay,simulation,integrator)
    delay=4
    tmp=openmmMC.integrators.applyRDF(coeff,delay,simulation,integrator)
    trjx[len(trjx):]= tmp
    tmp=openmmMC.integrators.applyRDF(coeff,delay,simulation,integrator)
    trjx[len(trjx):]= tmp
    tmp=openmmMC.integrators.applyRDF(coeff,delay,simulation,integrator)
    trjx[len(trjx):]= tmp
    tmp=openmmMC.integrators.applyRDF(coeff,delay,simulation,integrator)
    trjx[len(trjx):]= tmp
    
    for i in range(MDStep):
        simulation.step(1)
        trjx.append(integrator.getx)

    openmmMC.reporters.HDF5Reporter('testRDFMDout.h5', trjx)
    # a=np.array(trjx).transpose()
    # plt.plot(range(len(a[1])), a[1], '-r', label='trjx_2')
    # plt.show()


if __name__ == '__main__':
    fname = 'filterLowpass0_005.txt'
    fname = 'filterLowEnhance0_005.txt'
    DFMD(fname)
    MDFreq('testLowEnhanceDFMDout.h5', cut=2000, padding=5130)
    #NVE(MDSteps=10000, dt=0.1*unit.femtoseconds)
    #MDFreq('NVEtest.h5')
   
