import os.path
import shutil

class SimpleParameterFile(object):
    """
    Handles parameter files in a simple "name = value" format, with no nesting or grouping.
    """

    def __init__(self,initialiser):
        """
            Create a new parameter set from a file or string. In both cases,
            parameters should be separated by newlines.
        """
        self.values = {}
        self.types = {}
        self.comments = {}
        if isinstance(initialiser, dict):
            # initialiser is a dictionary
            for name, value in initialiser.items():
                self.values[name] = value
                self.types[name] = type(value)
        elif isinstance(initialiser, basestring):
            try:
                path_exists = os.path.exists(initialiser)
            except Exception:
                path_exists = False

            if path_exists:
                # initialiser is a filename
                with open(initialiser) as f:
                    content = f.readlines()
                self.source_file = initialiser
            else: 
                # initialiser is not a filename but is a string stream
                content = initialiser.split("\n")

            # read content
            for line in content:
                line = line.strip()
                if "=" in line:
                    parts = line.split("=")
                    name = parts[0].strip()
                    value = "=".join(parts[1:])
                    try:
                        self.values[name] = eval(value)
                    except NameError:
                        self.values[name] = unicode(value)
                    except TypeError as err:
                        raise SyntaxError("File is not a valid simple paramter file. %s" % err)
                    if "#" in value:
                        commnet = "#".join(value.split("#")[1:]) # this fails if the value is a string containing '#'
                        self.comments[name] = comment
                    self.types[name] = type(self.values[name])
                elif line:
                    if line.strip()[0] == "#":
                        pass
                    else:
                        raise SyntaxError("File is not a valid simple parameter file. This line caused the error: %s" % line)
        else:
            raise TypeError("Parameter set initialiser must be a filename, string or dict.")

    def __str__(self):
        return self.pretty()

    def __getitem__(self, name):
        return self.values[name]

    def __eq__(self, other):
        return ((self.values == other.values) and (self.types == other.types))

    def __ne__(self, other):
        return not self.__eq__(other)

    def pop(self, k, d=None):
        if k in self.values:
            v = self.values.pop(k)
            self.types.pop(k)
            self.comments.pop(k, None)
            return v
        elif d:
            return d
        else:
            raise KeyError("%s not found" % k)

    def pretty(self, expand_urls=False):
        """
        Return a string representation of the parameter set, suitable for
        creating a new, identical parameter set.

        expand_urls is present for compatibility with NTParameterSet, and is
        not used.
        """
        output = []
        for name, value in self.values.items():
            type = self.types[name]
            if issubclass(type, basestring):
                output.append('%s = "%s"' % (name, value))
            else:
                output.append('%s = %s' % (name, value))
            if name in self.comments:
                output[-1] += ' #%s' % self.comments[name]
        return "\n".join(output)

    def as_dict(self):
        return self.values.copy()

    def save(self, filename, add_extension=False):
        if add_extension:
            filename += ".param"
        if os.path.exists(filename):
            shutil.copy(filename, filename + ".orig")
        with open(filename, 'w') as f:
            f.write(self.pretty())
        return filename

    def update(self, E, **F):
        __doc__ = dict.update.__doc__
        def _update(name, value):
            if not isinstance(value, (int, float, string_type, list)):
                raise TypeError("value must be a numeric value or a string")
            self.values[name] = value
            self.types[name] = type(value)
        if hasattr(E, "items"):
            for name,value in E.items():
                _update(name, value)
        else:
            for name, value in E:
                _update(name, value)
        for name,value in F.items():
            _update(name, value)

