#=============================================================================================
# MODULE DOCSTRING
#=============================================================================================

"""
Test custom integrators.
"""

#=============================================================================================
# GLOBAL IMPORTS
#=============================================================================================

import numpy as np
from simtk import unit
from simtk import openmm
from simtk.openmm import app
import openmmMC
from openmmtools import integrators, testsystems
from openmmplumed import PlumedForce
from sys import stdout
import mdtraj.reporters
import numpy
import re
import os


#=============================================================================================
# CONSTANTS
#=============================================================================================

kB = unit.BOLTZMANN_CONSTANT_kB * unit.AVOGADRO_CONSTANT_NA

#=============================================================================================
# UTILITY SUBROUTINES
#=============================================================================================

def check_stability(integrator, test, platform=None, nsteps=100, temperature=300.0*unit.kelvin):
   """
   Check that the simulation does not explode over a number integration steps.
   Parameters
   ----------
   integrator : simtk.openmm.Integrator
      The integrator to test.
   test : testsystem
      The testsystem to test.
   """
   kT = kB * temperature

   # Create Context and initialize positions.
   if platform:
      context = openmm.Context(test.system, integrator, platform)
   else:
      context = openmm.Context(test.system, integrator)
   context.setPositions(test.positions)
   context.setVelocitiesToTemperature(temperature) # TODO: Make deterministic.

   # Take a number of steps.
   integrator.step(nsteps)

   # Check that simulation has not exploded.
   state = context.getState(getEnergy=True)
   potential = state.getPotentialEnergy() / kT
   if numpy.isnan(potential):
      raise Exception("Potential energy for integrator %s became NaN." % integrator.__doc__)

   del context

   return

#=============================================================================================
# TESTS
#=============================================================================================

def test_stabilities_harmonic_oscillator():
   """
   Test integrators for stability over a short number of steps of a harmonic oscillator.
   """
   test = testsystems.HarmonicOscillator()

   for methodname in dir(integrators):
      if re.match('.*Integrator$', methodname):
         integrator = getattr(integrators, methodname)()
         integrator.__doc__ = methodname
         check_stability.description = "Testing %s for stability over a short number of integration steps of a harmonic oscillator." % methodname
         yield check_stability, integrator, test

def test_trj():
   testsystem = testsystems.HarmonicOscillator()
   system, topology = testsystem.system, testsystem.topology
   temperature = 298.0 * unit.kelvin
   integrator = integrators.HMCIntegrator()
   simulation = app.Simulation(topology, system, integrator)
   simulation.context.setPositions(testsystem.positions)
   simulation.context.setVelocitiesToTemperature(temperature)
   simulation.reporters.append(mdtraj.reporters.HDF5Reporter('out.h5',5))
   simulation.reporters.append(app.PDBReporter('out.pdb',5))
   for i in range(5):
      simulation.step(5)
      print np.array(integrator.gettrj())*10

def HMC_Harmonic():
   testsystem = testsystems.HarmonicOscillator()
   system, topology = testsystem.system, testsystem.topology
   temperature = 300.0 * unit.kelvin
   integrator = integrators.HMCIntegrator()
   simulation = app.Simulation(topology, system, integrator)
   simulation.context.setPositions(testsystem.positions)
   simulation.context.setVelocitiesToTemperature(temperature)
   simulation.reporters.append(app.StateDataReporter(stdout, 1, step=True))
   simulation.reporters.append(mdtraj.reporters.HDF5Reporter('harmonic1.h5',100))
   simulation.step(10)

def HMC_aladipImplicit():
   testsystem = testsystems.AlanineDipeptideImplicit()
   system, topology = testsystem.system, testsystem.topology
   
   temperature = 300.0 * unit.kelvin
   
   integrator = openmmMC.HMCIntegrator(temperature=temperature, timestep=2*unit.femtoseconds)
   
   platform = openmm.Platform.getPlatformByName('CUDA')
   
   simulation = app.Simulation(topology, system, integrator, platform)
   
   simulation.context.setPositions(testsystem.positions)
   simulation.context.setVelocitiesToTemperature(temperature)
   simulation.reporters.append(app.StateDataReporter(stdout, 10000, step=True))
   simulation.reporters.append(mdtraj.reporters.HDF5Reporter('aladipImplicit.h5',1000))
   simulation.step(10000000)

def HMC_aladipExplicit():
   testsystem = testsystems.AlanineDipeptideExplicit()
   system, topology = testsystem.system, testsystem.topology
   
   temperature = 300.0 * unit.kelvin
   
   integrator = openmmMC.HMCIntegrator(temperature=temperature, timestep=2*unit.femtoseconds)
   
   platform = openmm.Platform.getPlatformByName('CUDA')
   
   simulation = app.Simulation(topology, system, integrator, platform)
   
   simulation.context.setPositions(testsystem.positions)
   simulation.context.setVelocitiesToTemperature(temperature)
   simulation.reporters.append(app.StateDataReporter(stdout, 10000, step=True))
   simulation.reporters.append(mdtraj.reporters.HDF5Reporter('aladipExplicit.h5',100))
   simulation.step(100000)

def MD_aladipImplicit():
   testsystem = testsystems.AlanineDipeptideImplicit()
   system, topology = testsystem.system, testsystem.topology
   
   temperature = 300.0 * unit.kelvin
   
   integrator = openmm.LangevinIntegrator(temperature, 1/unit.picoseconds, 2*unit.femtoseconds)
   
   platform = openmm.Platform.getPlatformByName('CUDA')
   
   simulation = app.Simulation(topology, system, integrator, platform)
   
   simulation.context.setPositions(testsystem.positions)
   simulation.context.setVelocitiesToTemperature(temperature)
   simulation.reporters.append(app.StateDataReporter(stdout, 1000000, step=True))
   simulation.reporters.append(mdtraj.reporters.HDF5Reporter('aladipImplicitMD.h5',1000))
   simulation.step(100000000)

def Plumed_aladipVacuum(plumedDat, nSteps=500000):
    folder=os.path.dirname(plumedDat)
    fpath = os.path.join(folder,'aladipVacuum.h5')
    testsystem = testsystems.AlanineDipeptideVacuum()
    system, topology = testsystem.system, testsystem.topology
    temperature = 300.0 * unit.kelvin

    dt=2*unit.femtoseconds

    # Plumed
    plumed_cmd= readPlumedDat(plumedDat)
    metad = PlumedForce(plumed_cmd)
    system.addForce(metad)

    integrator = openmm.LangevinIntegrator(temperature, 1/unit.picoseconds, 2*unit.femtoseconds)

    platform = openmm.Platform.getPlatformByName('CUDA')

    simulation = app.Simulation(topology, system, integrator, platform)

    simulation.context.setPositions(testsystem.positions)
    simulation.context.setVelocitiesToTemperature(temperature)
    simulation.reporters.append(app.StateDataReporter(stdout, 100000, step=True))
    simulation.reporters.append(mdtraj.reporters.HDF5Reporter(fpath,100))
    simulation.step(nSteps)

def Plumed_aladipImplicit(plumedDat, nSteps=500000):
    folder=os.path.dirname(plumedDat)
    fpath = os.path.join(folder,'aladipImplicitUS.h5')
    testsystem = testsystems.AlanineDipeptideImplicit()
    system, topology = testsystem.system, testsystem.topology
    temperature = 300.0 * unit.kelvin

    dt=2*unit.femtoseconds

    # Plumed
    plumed_cmd= readPlumedDat(plumedDat)
    metad = PlumedForce(plumed_cmd)
    system.addForce(metad)

    integrator = openmm.LangevinIntegrator(temperature, 1/unit.picoseconds, 2*unit.femtoseconds)

    platform = openmm.Platform.getPlatformByName('CPU')

    simulation = app.Simulation(topology, system, integrator, platform)

    simulation.context.setPositions(testsystem.positions)
    simulation.context.setVelocitiesToTemperature(temperature)
    simulation.reporters.append(app.StateDataReporter(stdout, 100000, step=True))
    simulation.reporters.append(mdtraj.reporters.HDF5Reporter(fpath,100))
    simulation.step(nSteps)

def readPlumedDat(fpath):
    with open(fpath,'r') as f:
      plumedlist = f.readlines()
      plumedlist = [ i for i in plumedlist if i.startswith('#') == False ]
      plumed_cmd = "".join(j for j in plumedlist)
    return plumed_cmd


if __name__ == '__main__':
   Plumed_aladipImplicit("plumed.dat")

